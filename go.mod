module gitlab.com/macmv/curse-pack-extractor

go 1.15

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/vbauerster/mpb v3.4.0+incompatible // indirect
	github.com/vbauerster/mpb/v6 v6.0.2
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
