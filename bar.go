package main

import (
  "io"

  "github.com/logrusorgru/aurora"

  "github.com/vbauerster/mpb/v6"
  "github.com/vbauerster/mpb/v6/decor"
)

type filler struct {
}

func bar_filler() mpb.BarFiller {
  f := filler{}
  return &f
}

func (s *filler) Fill(w io.Writer, width int, stat decor.Statistics) {
  width -= 2 // brackets
  if width <= 0 { return }

  amount := int64(float64(stat.Current) / float64(stat.Total) * float64(width))

  line := "["
  for i := int64(0); i < amount; i++ { line += aurora.Green(aurora.Bold("=")).String() }
  line += aurora.Green(aurora.Bold(">")).String()
  for i := amount; i < int64(width); i++ { line += "-" }
  line += "]"

  io.WriteString(w, line)
}

type BarWriter struct {
  Total int64
  Bar *mpb.Bar
  current int64
}

func (w *BarWriter) Write(b []byte) (n int, e error) {
  prev_percent := int(float64(w.current) / float64(w.Total) * 100)
  w.current += int64(len(b))
  percent := int(float64(w.current) / float64(w.Total) * 100)
  if prev_percent != percent {
    w.Bar.IncrBy(percent - prev_percent)
  }
  return len(b), nil
}
