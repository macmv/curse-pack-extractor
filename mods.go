package main

import (
  "os"
  "fmt"
  "log"
  "net/http"
  "io"
  "encoding/json"
  "path/filepath"

  "github.com/vbauerster/mpb/v6"
)

type ModInfo struct {
  // Is the file id
  Id int64
  // The mod name
  Name string

  // List of authors
  Authors []struct {
    // Author name
    Name string
    // User URL
    Url string
    // Is the project ID
    ProjectId int64
    // ???
    Id int64
    // The curseforge user ID
    UserId int64
    // An id for the attached twitch user
    TwitchId int64
  }

  // User specified mod url
  WebsiteUrl string
  // ID for which game this is for
  GameId int64
  // Short mod summary
  Summary string
  // Is usually the latest version
  DefaultFileId int64
  // Number of times this has been downloaded
  DownloadCount int64

  // List of 'latest' files
  // Is usually a list of a single forge mod and a single fabric mod
  LatestFiles []FileInfo

}

type FileInfo struct {
  // File id
  Id int64
  // Is usually the same as the filename
  DisplayName string
  // Name of file on disk
  FileName string
  // Upload time (in the format "2019-01-07T20:37:37.717Z")
  FileDate string
  // Length in bytes
  FileLength int64
  // ??? (is usually 1-4)
  ReleaseType int64
  // ??? (is usually 4)
  FileStatus int64
  // Direct download url
  DownloadUrl string
  // Set when the alternate file id is valid
  IsAlternate bool
  // Can be used if the DownloadUrl does not work
  AlternateFileId int64
  // List of dependencies
  Dependencies []struct {
    // Project id
    AddonId int64
    // ???
    Type int64
  }
  // Should always be true
  IsAvailible bool

  // List of top level folders
  Modules []struct {
    // Can be file name
    Foldername string
    // Some number
    Fingerprint int64
    // ???
    Type int64
  }

  // Some number
  PackageFingerprint int64
  // List of versions this works on
  GameVersion []string
  // ???
  InstallMetadata interface{}
  // ???
  Changelog interface{}
  // Why is this a thing?
  HasInstallScript bool
  // Should always be true
  IsCompatibleWithClient bool
  // ???
  CategorySectionPackageType int64
  // ???
  RestrictProjectFileAccess int64
  // ???
  ProjectStatus int64
  // ???
  RenderCacheId int64
  // ???
  FileLegacyMappingId interface{}
  // The project ID (should be same as above)
  ProjectId int64
  // Another project id, can be null
  ParentProjectFileId interface{}
  // ???
  ParentFileLegacyMappingId interface{}
  // ???
  FileTypeId interface{}
  // ???
  ExposeAsAlternative interface{}
  // Somehow different from the PackageFingerprint?
  PackageFingerprintId int64
  // The date the minecraft version was released (same date format as above)
  GameVersionDateReleased string
  // ???
  GameVersionMappingId int64
  // Some sort of curseforge id for the game version
  GameVersionId int64
  // Curseforge id for the game
  GameId int64
  // ???
  IsServerPack bool
  // ???
  ServerPackFileId interface{}
}

func get_mod_info(project_id, file_id int64) *FileInfo {
  url := fmt.Sprintf(
    "https://addons-ecs.forgesvc.net/api/v2/addon/%v/file/%v",
    project_id,
    file_id,
  )

  res, err := http.Get(url)
  if err != nil { log.Fatal(err) }
  defer res.Body.Close()

  j := json.NewDecoder(res.Body)
  var info FileInfo
  j.Decode(&info)

  return &info
}

func download_mod(bar *mpb.Bar, path string, f *FileInfo) {
  path = filepath.Join(path, "mods", f.FileName)

  res, err := http.Get(f.DownloadUrl)
  if err != nil { log.Fatal(err) }
  defer res.Body.Close()

  os.MkdirAll(filepath.Dir(path), 0755)
  file, err := os.Create(path)
  if err != nil { log.Fatal(err) }
  defer file.Close()

  w := &BarWriter{}
  w.Total = f.FileLength
  w.Bar = bar
  _, err = io.Copy(file, io.TeeReader(res.Body, w))
  if err != nil { log.Fatal(err) }
}

