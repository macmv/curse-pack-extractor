package main

import (
  "io"
  "os"
  "fmt"
  "log"
  "os/exec"
  "strconv"
  "strings"
  "net/http"
  "path/filepath"

  "github.com/vbauerster/mpb/v6"
  "github.com/vbauerster/mpb/v6/decor"
)

func install_forge_server(path string, manifest *Manifest) {
  installer_path := download_forge_server(manifest)

  cmd := exec.Command("java", "-jar", installer_path, "--installServer", path)
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  err := cmd.Run()
  if err != nil { log.Fatal(err) }
}

func download_forge_server(manifest *Manifest) string {
  version := ""
  mc_version := manifest.Minecraft.Version
  for _, l := range manifest.Minecraft.ModLoaders {
    if l.Primary {
      version = l.Id
      break
    }
  }
  if strings.HasPrefix(version, "forge-") {
    version = version[len("forge-"):]
  }
  filename := fmt.Sprintf(
    "forge-%v-%v-installer.jar",
    mc_version,
    version,
  )
  installer_path := filepath.Join("/tmp", filename)
  url := fmt.Sprintf(
    "https://files.minecraftforge.net/maven/net/minecraftforge/forge/%v-%v/%v",
    mc_version,
    version,
    filename,
  )
  res, err := http.Get(url)
  if err != nil { log.Fatal(err) }
  defer res.Body.Close()

  f, err := os.Create(installer_path)
  if err != nil { log.Fatal(err) }

  log.Printf("Downloading forge %v for minecraft %v...\n", version, mc_version)

  progress := mpb.New(mpb.WithWidth(64))
  bar := progress.Add(
    100,
    bar_filler(),
    mpb.PrependDecorators(
      decor.Name(filename),
      decor.Percentage(decor.WCSyncSpace),
    ),
    mpb.AppendDecorators(
      decor.OnComplete(
        decor.EwmaETA(decor.ET_STYLE_GO, 60), "Done",
      ),
    ),
  )

  length, _ := strconv.Atoi(res.Header.Get("content-length"))

  w := &BarWriter{}
  w.Total = int64(length)
  w.Bar = bar
  _, err = io.Copy(f, io.TeeReader(res.Body, w))

  f.Close()

  progress.Wait()

  return installer_path
}
