package main

import (
  "io"
  "fmt"
  "log"
  "sync"
  "encoding/json"

  "github.com/vbauerster/mpb/v6"
  "github.com/vbauerster/mpb/v6/decor"
)

type Manifest struct {
  Minecraft struct{
    // 1.16.4, for example
    Version string
    // Should be a list of valid loaders
    ModLoaders []struct{
      // forge-35.1.37, for example
      Id string
      // Set to true for one of the loaders
      Primary bool
    }
  }
  // Should be 'minecraftModpack'
  ManifestType string
  // The overrides folder
  Overrides string
  // Should be 1
  ManifestVersion int
  // Modpack version, can be anything
  Version string
  // Modpack author, can be anything
  Author string
  // Modpack name, can be anything
  Name string
  // A list of curseforge mods
  Files []struct{
    // The project ID in curseforge
    ProjectID int64
    // The file ID (id for different versions of a mod)
    FileID int64
    // Set to true in most situations
    Required bool
  }
}

func download_manifest(path string, manifest_src io.Reader) *Manifest {
  json := json.NewDecoder(manifest_src)
  manifest := Manifest{}
  json.Decode(&manifest)

  // Download the json info about each mod
  mods := make([]*FileInfo, len(manifest.Files))
  log.Println("Downloading mod info...")
  var wg sync.WaitGroup
  for i, f := range manifest.Files {
    wg.Add(1)
    go func(index, project_id, file_id int64) {
      mods[index] = get_mod_info(project_id, file_id)
      wg.Done()
    }(int64(i), f.ProjectID, f.FileID)
  }
  wg.Wait()

  // Find the max name width, for padding
  max_name_width := 0
  for _, f := range mods {
    if len(f.DisplayName) > max_name_width {
      max_name_width = len(f.FileName)
    }
  }

  // Download each jar file for all the mods
  log.Println("Downloading mods...")
  wg = sync.WaitGroup{}
  progress := mpb.New(mpb.WithWaitGroup(&wg), mpb.WithWidth(64))
  for _, f := range mods {
    wg.Add(1)
    name := fmt.Sprintf(
      fmt.Sprintf("%%%vv", max_name_width),
      f.FileName,
    )
    bar := progress.Add(
      100,
      bar_filler(),
      mpb.PrependDecorators(
        decor.Name(name),
        decor.Percentage(decor.WCSyncSpace),
      ),
      mpb.AppendDecorators(
        decor.OnComplete(
          decor.EwmaETA(decor.ET_STYLE_GO, 60), "Done",
        ),
      ),
    )

    go func(bar *mpb.Bar, f *FileInfo) {
      download_mod(bar, path, f)
      wg.Done()
    }(bar, f)
  }
  progress.Wait()
  log.Println("Done downloading mods")

  return &manifest
}
