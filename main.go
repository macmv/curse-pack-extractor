package main

import (
  "io"
  "os"
  "log"
  "flag"
  "bytes"
  "strings"
  "net/http"
  "io/ioutil"
  "archive/zip"
  "path/filepath"
)

var (
  flag_url = flag.String("url", "", "Pack zip url")
  flag_zip = flag.String("zip", "", "Pack zip location on disk")
  flag_path = flag.String("path", "server", "Extraction location")
)

func main() {
  flag.Parse()
  if *flag_url == "" && *flag_zip == "" {
    log.Fatal("Please enter a valid pack location (use either -url or -zip)")
  } else if *flag_url != "" && *flag_zip != "" {
    log.Fatal("Please enter only a url of zip file, not both")
  }

  var reader io.ReaderAt
  var size int64
  if *flag_url != "" {
    log.Printf("Downloading zip from url %v...\n", *flag_url)
    reader, size = download_zip(*flag_url)
  } else {
    log.Printf("Opening zip from file %v...\n", *flag_zip)
    var file *os.File
    file, size = load_zip(*flag_zip)
    defer file.Close()
    reader = file
  }

  r, err := zip.NewReader(reader, size)
  if err != nil { log.Fatal(err) }

  var manifest *Manifest
  for _, f := range r.File {
    if f.Name == "manifest.json" {
      r, err := f.Open()
      if err != nil { log.Fatal(err) }

      manifest = download_manifest(*flag_path, r)
      break
    }
  }
  // We want to place all override files
  // after the manifest is extracted.
  for _, f := range r.File {
    if strings.HasPrefix(f.Name, manifest.Overrides) {
      path := filepath.Join(*flag_path, f.Name[len(manifest.Overrides):])

      if f.Mode().IsDir() {
        err := os.MkdirAll(path, 0755)
        if err != nil { log.Fatal(err) }
      } else {
        _, err := os.Stat(path)
        if err == nil {
          log.Println("Skipping", path, "as it already exists")
          continue
        }
        file, err := os.Create(path)
        if err != nil { log.Fatal(err) }

        r, err := f.Open()
        if err != nil { log.Fatal(err) }

        io.Copy(file, r)

        r.Close()
        file.Close()
      }

      //insert_override(*flag_path, f)
    }
  }

  // Download and install forge
  install_forge_server(*flag_path, manifest)
}

func download_zip(url string) (*bytes.Reader, int64) {
  res, err := http.Get(url)
  if err != nil { log.Fatal(err) }

  defer res.Body.Close()
  buffer, err := ioutil.ReadAll(res.Body)
  if err != nil { log.Fatal(err) }

  return bytes.NewReader(buffer), int64(len(buffer))
}

func load_zip(path string) (*os.File, int64) {
  f, err := os.Open(path)
  if err != nil { log.Fatal(err) }

  s, err := f.Stat()
  if err != nil { log.Fatal(err) }

  return f, s.Size()
}
